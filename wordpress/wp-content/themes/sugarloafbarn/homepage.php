<?php
/**
 * Template Name: Homepage
 *
 * 
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

<?php

    if ( have_posts() ) {        
        
        $pageId = $post->ID;
        $slideCount = 0;
        $slideNav = '';
         
    
        $slideshow = simple_fields_get_post_group_values($pageId , "Slideshow", true, 2);       
        
        if (!empty($slideshow)){
              
        echo '<div id="slideContainer" class="slide-container">';
            echo '<ul id="slideshow" class="bjqs">';
 

            foreach ($slideshow as $slide) {
                
                if (!empty($slide['Slide image'])) {
                    
                    $slideOutput = '';
                    $slideImg = '';
                    $slideUrl = '';
                    
                    $slideImg = wp_get_attachment_image_src($slide['Slide image'], 'full');                   
                    $slideOutput .= '<li class="slide" style="background: url(' . $slideImg[0]. ') no-repeat scroll 0 0 transparent">';    
                    
                    if(!empty($slide['Slide link'])) {
                        $slideUrl = get_permalink($slide['Slide link']);                      
                        $slideOutput .= '<a href="' . $slideUrl . '">';
                    }
                    
                    if(!empty($slide['Slide image heading text'])) {
                        
                        $slideOutput .= '<div class="textPanel"><h1>' . $slide['Slide image heading text']. '</h1></div>';
                    }
                    
                    if(!empty($slide['Slide link'])) {
                    
                        $slideOutput .= '</a>';
                        
                    }
                    
                    $slideOutput .= '</li>';
                    $slideCount++;
                    
                    echo $slideOutput;                     
                }               
            }
            echo '</ul>';
        echo '</div>';
?>  


<?php
        
        }

?>
    
    <div class="content">

<?php
        
        $bodyPanels = simple_fields_get_post_group_values($pageId , "Body panels", true, 2);
        $panelOutput = '';
        $panelCount = 0;
        if (!empty($bodyPanels)) {
           
?>
    
        <section class="body-widget-area clearfix">   
        
<?php
            foreach ($bodyPanels as $panel) { 

                if ($panelCount < 4) {
                    $panelOutput .= '<article class="body-widget">';

                    if (!empty($panel['Body Panel Heading'])) {
                        $panelOutput .= '<h2>' . $panel['Body Panel Heading']. '</h2>';    
                    }

                    if (!empty($panel['Body Panel Image'])) {
                        $panelImg = wp_get_attachment_image_src($panel['Body Panel Image'], 'full');
                        $panelOutput .= '<img src="' . $panelImg[0]. '">';
                    }

                    if (!empty($panel['Body Panel Text'])) {
                        $panelOutput .= '<p>' . $panel['Body Panel Text']. '</p>';
                    }

                    if (!empty($panel['Body Panel Link']) && !empty($panel['Body Panel Link Text'])) {
                        $panelOutput .= '<a href="' . get_permalink($panel['Body Panel Link']) . '" class="ctaButton silver">';
                        $panelOutput .= $panel['Body Panel Link Text'] . '  &#0187;';
                        $panelOutput .= '</a>';                       
                    }

                    $panelOutput .= '</article>';

                }

            }
            echo $panelOutput;
?>
        
        </section>
                
<?php        
        
        }
    
    }
?>
    <section class="aggregation-area">
        <h3>Latest News And Activities</h3>
        <div class="aggregationResults">        
<?php

    // global $post;
    $args = array( 'numberposts' => 2, 'orderby' => 'date' );
    $myposts = get_posts( $args );
    $postOutput = '';

    
    foreach( $myposts as $post ) : setup_postdata($post);
    	             
        $postOutput .= '<article class="aggregationItem clearfix">';
        
        if ( has_post_thumbnail()) {
            $postOutput .= '<a href="' . get_permalink() . '">';
            $postOutput .= get_the_post_thumbnail($post->ID, array(95, 46), array( 'class' => "aggregationThumb"));
            $postOutput .= '</a>'; 
        }
      
        $postOutput .= '<a href="' . get_permalink() . '">';
        $postOutput .= '<h4>' . get_the_title() . '</h4>';
        $postOutput .= '</a>';
        $postOutput .= '<span class="time">' . get_the_time('jS F Y') . '</span>';
        
        $excerpt = get_the_excerpt();
        if (!empty($excerpt)) {            
            
            $truncatedExcerpt = string_limit_words($excerpt, 45);
            $postOutput .= '<a href="' . get_permalink() . '">';
            $postOutput  .= '<p>' . $truncatedExcerpt . '</p>';
            $postOutput .= '</a>';      
            
        }

        $postOutput .= '</article>';                    
        
    endforeach;
    
    echo $postOutput;
?>
        </div>
    </section>
</div>
<?php get_footer(); ?>
