<?php
/**
 * Template Name: Aggregation
 *
 * i.e News 
 *
 * @package WordPress
 * @subpackage The Sugar Loaf Barn Theme
 * @since The Sugar Loaf Barn Theme 1.0
 */

get_header(); ?>

	<div class="main clearfix">
            <div class="mainCol">
                <div class="breadcrumbs">
                    <?php getBreadCrumb(); ?> 
                </div> 
                <div class="content bordered">
                <?php                       

                        echo '<h1 class="heading-text">' . get_the_title( get_option( 'page_for_posts' ) )  . '</h1>';                       
                        
                        $page_for_posts_id = get_option('page_for_posts');
                        
                        setup_postdata(get_page($page_for_posts_id));   
                        
                        echo the_content();
                        
                        rewind_posts();
                ?>
                </div>
                <div class="fullAggregation aggregationResults">
                <?php
                    if ( have_posts() ) :
                        while (have_posts()) : the_post();

                ?>
                <article class="aggregationItem clearfix"">
                    <a href="<?php the_permalink(); ?>">
                    <?php
                        if (has_post_thumbnail()) {
                            the_post_thumbnail( array(209, 105), array( 'class' => "aggregationThumb" ) ); 
                        }
                    ?>
                    </a>

                    <a href="<?php the_permalink(); ?>">
                        <h2><?php the_title(); ?></h2>
                    </a>

                    <a href="<?php the_permalink(); ?>">
                        <span class="time"><?php the_time('jS F Y'); ?></span>
                    </a>

                    <a href="<?php the_permalink() ?>">
                        <?php the_excerpt(); ?>
                    </a>
                </article>    
            
                <?php
                        endwhile;
                ?>
                    
                </div>            
                
                <?php
                    endif;
                ?>

<?php get_footer(); ?>