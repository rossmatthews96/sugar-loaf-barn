<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<head>
<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width" />
<title>The Sugar Loaf Barn</title>

<link rel="shortcut icon" href="http://staging.solidstategroup.com/global_business_initiative/wp-content/themes/gbi/images/gbi-favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" media="screen" href="css/reset.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/header.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/slideshow.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/nav.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/footer.css" />
</head>
<body>
    <div class="wrapper">
        <header>
            <div class="header-top clearfix">
                <a class="site-logo" title="Sugar Loaf Barn - Relax and unwind">Sugar Loaf Barn - Relax and unwind</a>
                <div class="social-media-icons">
                    <ul>
                        <li class="facebook"><a href="">Facebook</a></li>
                        <li class="twitter"><a href="">Twitter</a></li>
                    </ul>
                </div>            
                <p class="contact-info">Bookings And Reservations:  +44 01552 832 789</p>
            </div>
            <nav class="clearfix mainNav">
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="">THINGS TO DO</a></li>
                    <li><a href="accomodation.php">ACCOMODATION</a></li>
                    <li><a href="">LOCATION</a></li>
                    <li><a href="aggregation.php">NEWS</a></li>
                    <li><a href="gallery.php">GALLERY</a></li>
                    <li><a href="">RESERVATIONS</a></li>
                    <li><a href="contact.php">CONTACT</a></li>					
                </ul>
            </nav>
        </header>
        <div class="main clearfix">
            <div class="slide-container">
                <section id="slideshow">
                    <article class="slide" style="background: url('images/hills-slide.jpg') 0 0 transparent no-repeat;">
                        <div class="textPanel">
                            <hgroup>
                                <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</h1>
                            </hgroup>
                        </div>
                    </article>
                    <div class="sliderControls">
                        <span class="next">next</span>
                        <span class="previous">previous</span>
                    </div>
                    <ul>
                        <li><a class="current" href="">1</a></li>
                        <li><a class="" href="">2</a></li>
                        <li><a class="" href="">3</a></li>
                        <li><a class="" href="">4</a></li>                                
                    </ul>
                </section>
            </div>
            <div class="content">
                <section class="body-widget-area clearfix">
                    <article class="body-widget">
                        <h2>Welcome</h2>
                        <img src="images/bed.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        <a class="ctaButton silver">Find Out More &#0187;</a>
                    </article>
                    <article class="body-widget">
                        <h2>Things To Do</h2>
                        <img src="images/bed.jpg" alt="">                       
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        <a class="ctaButton silver">Find Out More &#0187;</a>
                    </article>
                    <article class="body-widget">
                        <h2>Accomodation</h2>
                        <img src="images/bed.jpg" alt="">                        
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        <a class="ctaButton silver">Find Out More &#0187;</a>
                    </article>					
                </section>
                <section class="aggregation-area">
                    <h3>Latest News And Activities</h3>
                    <div class="aggregationResults">
                        <article class="aggregationItem clearfix">
                                <img class="aggregationThumb" src="images/rider-thumb-small.jpg">
                                <h4>Lorem ipsum</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </article>
                        <article class="aggregationItem clearfix last">
                                <img class="aggregationThumb" src="images/rider-thumb-small.jpg">
                                <h4>Lorem ipsum</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </article>
                    </div>
                </section>
            </div>
            <footer class="page-footer clearfix">
                <p>COPYRIGHT 2011 THE SUGAR LOAF BARN SITE DESIGN AND DEVELOPMENT ROSS MATTHEWS 2012 POWERED BY WORDPRESS</p>
            </footer>
        </div>
    </div>
</body>
</html>
