project.register('utils', {

    sendRedirect: function(url) {
        window.location = project.pageContext + url;
    },

    /**
     * will check if a value has been set that it is a number and the number is > 0
     * @param value Integer value to check
     */
    validateInt: function(value) {
        return (value && !isNaN(value) && value.toInt() > 0);
    },

    /**
     * will check if a value has been set and its length is > 0
     * @param value String value to check
     */
    validateString: function(value) {
        return (value && value.length > 0);
    },

    getFunction: function(prop) {
        if (instanceOf(prop, Function)) {
            return prop;
        } else if (instanceOf(prop, String)) {
            if (prop) {
                var props = prop.split('.');

                var obj = window;
                for (var i = 0; i < props.length; i++) {
                    if (typeof prop != "undefined") {
                        obj = obj[props[i]]; // go next level
                    }
                }

                return obj;
            }
        }
        return $.noop();
    },

    clean: function(string) {
        return string.replace(/\s+/g, '').replace('&', '');
    }

});