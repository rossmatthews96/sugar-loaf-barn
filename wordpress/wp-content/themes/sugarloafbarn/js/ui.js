sugarloafbarn.register('ui', {

    init : function() {

        // main ui components - i.e. nav, lightboxes
        //this.setupYoutubeFix();

        // basic ui components - i.e. external links, clear inputs
        //this.setupExternalLinks();
        //this.setupDatePicker();
        //this.setupWholeItemAsLink();
        //this.setupClearFields();
        //this.setupFormValidation();

        // complicated ui components broken down into seperate files - i.e. heroarea sliders
        this.setupSlideShow($('#slideContainer'));
        this.setupGallery($('.gallery'));
    },

    // Opens link with .external class in a new window
//    setupExternalLinks: function () {
//        $('a.external').click(function(e) {
//            e.preventDefault();
//            window.open(this.href);
//            return false;
//        });
//        return this;
//    },

    // this makes a whole article result link to the 'more' page
    // i.e. on aggregation page, instead of just clicking on 'more', you can click anywhere on the .article item
//    setupWholeItemAsLink: function() {
//        var aggregatorArticles = $('.aggregator .article');
//
//        if (aggregatorArticles.length) {
//            aggregatorArticles.each(function() {
//                var el = $(this),
//                    link = el.find('.btn');
//
//                if (link.length) {
//                    el.click(function(e) {
//                        e.preventDefault();
//                        window.location.href = link.first().prop('href');
//                    });
//                }
//            });
//        }
//    },

    setupSlideShow : function(element){
        element.bjqs({
            width           : 920,
            height          : 380,
            animspeed       : 4000,
            showmarkers     : true,
            centermarkers   : false 
        });    
    },
    
    setupGallery : function(element){
        element.magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: { 
                enabled: true 
            },
            image: {
                titleSrc: 'title'
            }           
        });
    }
    
//    // setting up a google map 
//    setupGoogleMaps : function(el, lat, lng, containerSelector) {
//        if (!el) {
//            el = $('body');
//        }
//        var mapAreas = el.find(containerSelector);
//
//        if (mapAreas.length) {
//            mapAreas.each(function(index, el) {
//                if (lat && lng) {
//                    var latlng = new google.maps.LatLng(lat, lng);
//                    var map = new google.maps.Map(el, {
//                        zoom: 14,
//                        center: latlng,
//                        mapTypeId: google.maps.MapTypeId.ROADMAP,
//                        panControl: false,
//                        zoomControl: true,
//                        mapTypeControl: false
//                    });
//                    var marker = new google.maps.Marker({
//                        position: latlng,
//                        map: map
//                    });
//                }
//            });
//        }
//    },

    // adds search in the searchbox
//    setupClearFields: function() {
//        // For IE mostly
//        if (!Modernizr.placeholder) {
//            var fields = $('.clearField');
//
//            fields.each(function() {
//                var el = $(this),
//                    val = el.prop('placeholder');
//
//                el.prop('value', val);
//                el.on({
//                    click: function() {
//                        if (el.value === val) {
//                            el.prop('value', '');
//                            el.focus();
//                        }
//                    },
//                    blur: function() {
//                        if (el.value === '') {
//                            el.prop('value', val);
//                        }
//                    }
//                });
//            });
//        }
//    },

//    setupFormValidation : function () {
//
//        var validationForms = $('.validate');
//
//        if(validationForms){
//            validationForms.validate({
//                errorClass: "help-inline",
//                errorElement: "div",
//                highlight: function(element) {
//                    $(element).closest('.control-group').removeClass('success').addClass('error');
//                },
//                success: function(element) {
//                    element.addClass('valid').closest('.control-group').removeClass('error').addClass('success');
//                },
//                rules: {
//                    property_EmailAddress: {
//                      required: true,
//                      email: true
//                    }
//                }
//            });
//            $('.validate-integer').each(function (item) {
//                $(this).rules("add", {
//                    number: true
//                });
//            });
//        }
//    }

});


