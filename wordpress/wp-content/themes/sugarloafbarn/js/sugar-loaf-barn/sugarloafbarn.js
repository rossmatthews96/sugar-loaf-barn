var sugarloafbarn = {

    debug: true,

    pageContext : '',

    pages : {
        // specific page js here 
        reservationsPage: false 
    },

    formConfigs : {},

    /**
     * add new form configs in other js scripts
     * @param obj
     */
    addFormConfig : function(obj) {
        this.formConfigs = $extend(obj, this.formConfigs);
    },

    getFormConfig : function(formId) {
        return this.formConfigs[formId] || {};
    },

    /**
     * add new objects to a namespace
     * @param ns
     * @param object
     */
    register: function(ns, object) {
        var props = ns.split('.');
        var obj = this;

        /**
         * loop down the tree from window get the prop for each part of the namespace
         * if one doesnt exist just create an empty object and use that
         * if we are at the final part of the namespace set this to the object passed in
         */
        for (var i = 0; i < props.length; i++) {
            if (i == props.length - 1) {
                obj = obj[props[i]] = object;
            } else if (jQuery.type(obj[props[i]]) === 'null') {
                obj = obj[props[i]] = {};
            } else {
                obj = obj[props[i]];
            }
        }
    },   

    /**
     * our main load method for the app
     *
     * this is the only thing that should be called on domready this in turn should call every thing else
     * some things may not have actually been loaded yet through the registry so actually return a function to call
     */
    load : function() {

        // set up ui elements
        sugarloafbarn.ui.init();
        
       // example of loading specific page js
        if (this.pages.reservationsPage) {
            sugarloafbarn.pages.reservations.init();
        }

    },

    log : function(msg) {
        if (this.debug && window.console) {
            window.console.log(msg);
        }
    }
};
//
//sugarloafbarn.register('ui', {
//    init : function() {
//
//        // main ui components - i.e. nav, lightboxes
//        this.dropDownNav();
//        // basic ui components - i.e. external links, clear inputs
//
//        // complicated ui components broken down into seperate files - i.e. heroarea sliders
//        this.setupSlideShow();
//     },

//    dropDownNav: function() {
//        var menuItem = $$('nav.mainNav ul:first-child li');
//        
//        menuItem.each(function(item){
//            var subMenu = [];
//            subMenu = item.getChildren('ul');
//            
//            if (subMenu.length > 0){ 
//                item.addEvent('mouseenter', function(event) {
//
//                    subMenu.each(function(item) {
//                        item.setStyle('display', 'block');    
//                    });
//                }),
//
//                item.addEvent('mouseleave', function(event) {
//                    subMenu.each(function(item) {
//                        item.setStyle('display', 'none');    
//                    });
//                }) 
//            }
//        });
//                
//        
//    },
    
//    setupSlideShow: function() {
//
//        var heroSlider = $('slideshow');
//
//        if (heroSlider) {
//            var navs = $$('.slide-container ul li a');
//            var nextBtn = $$('.sliderControls .next');
//            var previousBtn = $$('.sliderControls .previous');
//
//            var runSlideShow = new SlideShow('slideshow', {
//                autoplay: true,
//                delay: 7000,
//                duration: 800,
//                selector: '.slide',
//		        transition: 'pushLeft',
//                onShow: function(data){
//                    navs[data.previous.index].removeClass('current');
//                    navs[data.next.index].addClass('current');
//                }
//            });
//
//            navs.each(function(item, index){
//                item.addEvent('click', function(event){
//                    event.stop();
//                    // pushLeft or pushRight, depending upon where
//                    // the slideshow already is, and where it's going
//                    var transition = (runSlideShow.index < index) ? 'pushLeft' : 'pushRight';
//                    runSlideShow.show(index, {transition: transition});
//                });
//            });
//
//            previousBtn.addEvent('click', function(event){
//                event.stop();
//                runSlideShow.show('previous', {transition: 'pushLeft'});
//            });
//
//            nextBtn.addEvent('click', function(event){
//                event.stop();
//                runSlideShow.show('next', {transition: 'pushRight'});
//            });
//        }
//
//    }





